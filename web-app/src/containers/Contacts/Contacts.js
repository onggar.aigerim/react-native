import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ContactList from "../../components/ContactList/ContactList";
import Loader from "../../components/UI/Loader/Loader";
import { fetchContacts, showModal } from "../../store/actions";

const Contacts = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const contacts = useSelector(store => store.contacts);
  const loading = useSelector(state => state.loading);

  useEffect(() => {
    dispatch(fetchContacts())
  }, [dispatch]);

  const showInfo = (id) => {
    navigate(`/contact/${id}`);
    dispatch(showModal(true));
  };

  return <div>
    <Loader loading={loading}/>
    <ContactList
      contacts={contacts}
      onClick={showInfo}
    />
  </div>
};

export default Contacts;
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import FormContact from "../../components/FormContact/FormContact";
import Loader from "../../components/UI/Loader/Loader";
import { editContact } from "../../store/actions";

const EditContact = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const contactPrev = useSelector(store => store.contact);
  const loading = useSelector(state => state.loading);

  const [contact, setContact] = useState({
    name: "",
    phone: "",
    email: "",
    photo: ""
  });

  useEffect(() => {
    setContact({
      name: contactPrev.name,
      phone: contactPrev.phone,
      email: contactPrev.email,
      photo: contactPrev.photo
    })
  }, [contactPrev]);

  const onChangeHandler = (e) => {
    const { name, value } = e.target;
    setContact(prevState => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const cancelHandler = () => {
    navigate("/");
  };

  const saveNewInfo = () => {
    if (contact.name !== "" && contact.phone !== "" && contact.email !== "" && contact.photo !== "") {
      dispatch(editContact(contactPrev.id, contact, navigate));
    };
  };

  return <div>
    <Loader loading={loading}/>
    <FormContact 
      contact={contact}
      onChange={onChangeHandler}
      onClickSave={saveNewInfo}
      onClickCancel={cancelHandler}
      photo={contact.photo}
    />
  </div> 
};

export default EditContact;
import axios from "../axiosContacts";
import { ADD_CONTACT_ERROR, ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS, EDIT_CONTACT_ERROR, EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS, FETCH_CONTACT_ERROR, FETCH_CONTACT_REQUEST, FETCH_CONTACT_SUCCESS, GET_CONTACT_INFO_ERROR, GET_CONTACT_INFO_REQUEST, GET_CONTACT_INFO_SUCCESS, SHOW_MODAL, UPDATE_INFO } from "./actionTypes";

const addContactRequest = () => {
  return {type: ADD_CONTACT_REQUEST};
};

const addContactSuccess = () => {
  return {type: ADD_CONTACT_SUCCESS};
};

const addContactError = (error) => {
  return {type: ADD_CONTACT_ERROR, error};
};

const fetchContactRequest = () => {
  return {type: FETCH_CONTACT_REQUEST};
};

const fetchContactSuccess = (contacts) => {
  return {type: FETCH_CONTACT_SUCCESS, contacts};
};

const fetchContactError = (error) => {
  return {type: FETCH_CONTACT_ERROR, error};
};


const getContactInfoRequest = () => {
  return {type: GET_CONTACT_INFO_REQUEST};
};

const getContactInfoSuccess = (contact) => {
  return {type: GET_CONTACT_INFO_SUCCESS, contact};
};

const getContactInfoError = (error) => {
  return {type: GET_CONTACT_INFO_ERROR, error};
};

const editContactRequest = () => {
  return {type: EDIT_CONTACT_REQUEST};
};

const editContactSuccess = () => {
  return {type: EDIT_CONTACT_SUCCESS};
};

const editContactError = (error) => {
  return {type: EDIT_CONTACT_ERROR, error};
};

export const updateInfoContact = () => {
  return {type: UPDATE_INFO};
};

export const addContact = (contact, navigate) => {
  return async (dispatch) => {
    dispatch(addContactRequest())
    try {
      await axios.post("/contacts.json", contact);
      dispatch(addContactSuccess());
      navigate("/");
    } catch (e) {
      dispatch(addContactError(e));
    };
  };
};

export const fetchContacts = () => {
  return async (dispatch) => {
    dispatch(fetchContactRequest())
    try {
      const response = await axios.get("/contacts.json");
      const contacts = Object.keys(response.data).map(contact => {
        return {...response.data[contact], id: contact}
      });
      dispatch(fetchContactSuccess(contacts));
    } catch (e) {
      dispatch(fetchContactError(e));
    };
  };
};

export const getContact = (id) => {
  return async (dispatch) => {
    dispatch(getContactInfoRequest())
    try {
      const response = await axios.get(`/contacts/${id}.json`);
      const contact = {...response.data, id: id};
      dispatch(getContactInfoSuccess(contact));
    } catch (e) {
      dispatch(getContactInfoError(e));
    };
  };
};

export const editContact = (id, contact, navigate) => {
  return async (dispatch) => {
    dispatch(editContactRequest())
    try {
      await axios.put(`/contacts/${id}.json`, contact);
      dispatch(editContactSuccess());
      navigate("/")
    } catch (e) {
      dispatch(editContactError(e));
    };
  };
};

export const deleteContact = (id, navigate) => {
  return async (dispatch) => {
    try {
      await axios.delete(`/contacts/${id}.json`);
      navigate("/")
    } catch (e) {
      dispatch(editContactError(e));
    };
  };
};

export const showModal = (status) => {
  return {type: SHOW_MODAL, status};
};
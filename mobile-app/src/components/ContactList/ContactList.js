import { FlatList, StyleSheet, Text } from "react-native";
import ContactItem from "../ContactItem/ContactItem";

const ContactList = ({ 
  contacts, 
  onPressHandler
}) => {
  return <>
    <Text style={styles.text}>Contacts</Text>
    <FlatList
      data={contacts}
      style={styles.contacts}
      renderItem={({item}) => <ContactItem
        contact={item}
        onPress={onPressHandler}
      />}
      keyExtractor={item => item.id}    
    />
  </> 
};

const styles = StyleSheet.create({
  contacts: {
    width: "100%",
  },
  text: {
    fontSize: 20,
    marginVertical: 20,
  }
})

export default ContactList;
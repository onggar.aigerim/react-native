import { Link, NavLink } from "react-router-dom";
import "./Toolbar.css";

const Toolbar = () => {
  return <header className="Toolbar">
    <Link to="/" className="Logo">
      Contacts
    </Link>
    <nav className="NavigationItems">
      <li className="NavigationItem">
      <NavLink to="/new-contact">
        Add new contact
      </NavLink>
    </li>
    </nav>
  </header>
};

export default Toolbar;
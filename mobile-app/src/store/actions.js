import axios from "../axiosContacts";
import { CONTACTS_REQUEST, CONTACTS_SUCCESS, CONTACTS_ERROR, CONTACT_INFO_REQUEST, CONTACT_INFO_SUCCESS, CONTACT_INFO_ERROR, UPDATE_CONTACT_INFO } from "./actionTypes";

const fetchContactsRequest = () => {
  return {type: CONTACTS_REQUEST};
};

const fetchContactsSuccess = (contacts) => {
  return {type: CONTACTS_SUCCESS, contacts};
};

const fetchContactsError = (error) => {
  return {type: CONTACTS_ERROR, error};
};

const fetchContactInfoRequest = () => {
  return {type: CONTACT_INFO_REQUEST};
};

const fetchContactInfoSuccess = (contact) => {
  return {type: CONTACT_INFO_SUCCESS, contact};
};

const fetchContactInfoError = (error) => {
  return {type: CONTACT_INFO_ERROR, error};
};

export const updateContactInfo = () => {
  return {type: UPDATE_CONTACT_INFO};
};

export const fetchContacts = () => {
  return async (dispatch) => {
    dispatch(fetchContactsRequest());
    try {
      const response = await axios.get("/contacts.json");
      const contacts = Object.keys(response.data).map(contact => {
        return {...response.data[contact], id: contact};
      });
      dispatch(fetchContactsSuccess(contacts));
    } catch (e) {
      dispatch(fetchContactsError(e));
    };
  };
};

export const fetchContactInfo = (id) => {
  return async (dispatch) => {
    dispatch(fetchContactInfoRequest());
    try {
      const response = await axios.get(`/contacts/${id}.json`);
      const contact = {...response.data, id: id};
      dispatch(fetchContactInfoSuccess(contact));
    } catch (e) {
      dispatch(fetchContactInfoError(e));
    };
  };
};
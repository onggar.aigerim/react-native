import { CONTACTS_ERROR, CONTACTS_REQUEST, CONTACTS_SUCCESS, CONTACT_INFO_ERROR, CONTACT_INFO_REQUEST, CONTACT_INFO_SUCCESS, UPDATE_CONTACT_INFO } from "./actionTypes";

const initialState = {
  contacts: [],
  contact: [],
  error: null,
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case CONTACTS_REQUEST:
      return {...state};
    case CONTACTS_SUCCESS:
      return {...state, contacts: action.contacts};
    case CONTACTS_ERROR:
      return {...state, error: action.error};
    case CONTACT_INFO_REQUEST:
      return {...state};
    case CONTACT_INFO_SUCCESS:
      return {...state, contact: action.contact};
    case CONTACT_INFO_ERROR:
      return {...state, error: action.error};
    case UPDATE_CONTACT_INFO: 
      return {...state, contact: {}}
    default:
      return state;
  }
}

export default reducer;
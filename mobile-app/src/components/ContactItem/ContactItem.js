import {Text, Image, View, StyleSheet, TouchableWithoutFeedback} from "react-native";

const ContactItem = ({ contact, onPress }) => {
  return <TouchableWithoutFeedback
    onPress={() => onPress(contact.id)}
  >
    <View 
      style={styles.contactItem}
    >
      <Image
        source={{uri: contact.photo}}
        style={styles.image}
      />
      <Text 
        style={styles.text}
      >
        {contact.name}
      </Text>
    </View>
  </TouchableWithoutFeedback> 
  
};

const styles = StyleSheet.create({
  contactItem: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginVertical: 10,
    backgroundColor: "#eaf8ff",
    borderWidth: 1,
    borderColor: "#4f9dc4",
  },
  image: {
    width: 50, 
    height: 50, 
    marginRight: 10
  },
  text: {
    marginRight: 50,
    fontSize: 16
  }
});

export default ContactItem;
import { Outlet } from "react-router-dom";
import Toolbar from '../Toolbar/Toolbar';
import "./Layout.css";

const Layout = () => {
  return (
    <>
      <Toolbar />
      <main className="Layout-Content">
        <Outlet />
      </main>
      <footer className="footer">onggarai</footer>
    </>
  );
};

export default Layout;
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import Main from './src/containers/Main/Main';
import reducer from './src/store/reducer';

const store = configureStore({reducer});

export default function App() {
  return <Provider store={store}>
    <Main />
  </Provider>
};
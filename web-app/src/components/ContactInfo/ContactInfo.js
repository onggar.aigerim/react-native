import "./ContactInfo.css";

const ContactInfo = ({
  contact, 
  onClickCancel, 
  onClickEdit,
  onClickDelete
}) => {
  return <div className="Contact_info">
    <div className="Main_info">
      <div className="Contact_info_img">
        <img src={contact.photo} alt="Contact" />
      </div>
      <div className="Contact_info_text">
        <span>{contact.name}</span>
        <span>&#9742; {contact.phone}</span>
        <span>&#128231; {contact.email}</span>
      </div>
    </div>
    <div className="Contact_info_buttons">
      <button onClick={onClickEdit}>&#128394; Edit</button>
      <button onClick={onClickDelete}>&#128465; Delete</button>
    </div>
    <button 
      className="Contact_info_сancel"
      onClick={onClickCancel}
    >
      &#128281;
    </button>
  </div>
};

export default ContactInfo;
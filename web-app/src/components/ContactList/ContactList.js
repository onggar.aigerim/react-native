import "./ContactList.css"

const ContactList = ({ contacts, onClick }) => {
  return <div className="ContactList">
    {contacts.map((contact) => {
      return <div 
        className="Contact"
        onClick={() => onClick(contact.id)}
      >
        <div className="Contact_img">
          <img src={contact.photo} alt="Contact" />
        </div>
        <p>{contact.name}</p>
      </div>
    })}
  </div>
};

export default ContactList;
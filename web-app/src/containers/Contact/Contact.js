import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import ContactInfo from "../../components/ContactInfo/ContactInfo";
import Modal from "../../components/UI/Modal/Modal";
import { deleteContact, getContact, updateInfoContact } from "../../store/actions";

const Contact = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const contact = useSelector(store => store.contact);
  const show = useSelector(store => store.show);

  useEffect(() => {
    dispatch(getContact(params.id))
  }, [params.id, dispatch]);

  const closeHandler = () => {
    navigate("/");
    dispatch(updateInfoContact());
  };

  const editHandler = () => {
    navigate(`/edit/${params.id}`)
  };

  const deleteHandler = () => {
    dispatch(deleteContact(params.id, navigate));
  };

  return <>
    <Modal show={show} close={closeHandler}>
      <ContactInfo
        contact={contact}
        onClickCancel={closeHandler}
        onClickEdit={editHandler}
        onClickDelete={deleteHandler}
      />
    </Modal>
  </>
};

export default Contact;
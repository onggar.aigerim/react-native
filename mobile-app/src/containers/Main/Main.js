import { Alert, Image, Modal, Pressable, StyleSheet, Text, View } from 'react-native';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import ContactList from '../../components/ContactList/ContactList';
import { fetchContactInfo, fetchContacts, updateContactInfo } from '../../store/actions';
import ContactInfo from '../../components/ContactInfo/ContactInfo';

const Main = () => {

  const dispatch = useDispatch();
  const contacts = useSelector(state => state.contacts);
  const contact = useSelector(state => state.contact);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    dispatch(fetchContacts());
  }, []);

  const onPressHandler = (id) => {
    setModalVisible(true);
    dispatch(fetchContactInfo(id));
  };

  const onPressCancelHandler = () => {
    setModalVisible(!modalVisible);
    dispatch(updateContactInfo());
  };

  return <View style={styles.container}>
    <ContactList
      contacts={contacts} 
      onPressHandler={onPressHandler}
    />
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
      Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <ContactInfo
        contact={contact}
        onPress={onPressCancelHandler}
      />
    </Modal>
  </View>;
};

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

export default Main;
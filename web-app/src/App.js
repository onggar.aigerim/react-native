import { Route, Routes, BrowserRouter } from "react-router-dom";
import Layout from "./components/Layout/Layout";
import AddNewContact from "./containers/AddNewContact/AddNewContact";
import Contact from "./containers/Contact/Contact";
import Contacts from "./containers/Contacts/Contacts";
import EditContact from "./containers/EditContact/EditContact";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Layout />}>
          <Route path="/" element={<Contacts />} />
          <Route path="/new-contact" element={<AddNewContact />} />
          <Route path="/contact/:id" element={<Contact />} />
          <Route path="/edit/:id" element={<EditContact />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
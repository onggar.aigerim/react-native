import { ADD_CONTACT_ERROR, ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS, EDIT_CONTACT_ERROR, EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS, FETCH_CONTACT_ERROR, FETCH_CONTACT_REQUEST, FETCH_CONTACT_SUCCESS, GET_CONTACT_INFO_ERROR, GET_CONTACT_INFO_REQUEST, GET_CONTACT_INFO_SUCCESS, SHOW_MODAL, UPDATE_INFO } from "./actionTypes";

const initialState = {
  contacts: [],
  contact: {},
  loading: false,
  error: null,
  show: false
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_CONTACT_REQUEST:
      return {...state, loading: true};
    case ADD_CONTACT_SUCCESS:
      return {...state, loading: false};
    case ADD_CONTACT_ERROR:
      return {...state, loading: false, error: action.error};
    case FETCH_CONTACT_REQUEST:
      return {...state, loading: true};
    case FETCH_CONTACT_SUCCESS:
      return {...state, loading: false, contacts: action.contacts};
    case FETCH_CONTACT_ERROR:
      return {...state, loading: false, error: action.error};
    case GET_CONTACT_INFO_REQUEST:
      return {...state, loading: true};
    case GET_CONTACT_INFO_SUCCESS:
      return {...state, loading: false, contact: action.contact};
    case GET_CONTACT_INFO_ERROR:
      return {...state, loading: false, error: action.error};
    case SHOW_MODAL: 
      return {...state, show: action.status};
    case UPDATE_INFO: {
      return {...state, contact: {}};
    }
    case EDIT_CONTACT_REQUEST: 
      return {...state, loading: true};
    case EDIT_CONTACT_SUCCESS:
      return {...state, loading: false}; 
    case EDIT_CONTACT_ERROR:
      return {...state, loading: false, error: action.error}; 
    default:
      return state;
  }
};

export default reducer;
import "./FormContact.css";

const FormContact = ({
  contact, 
  onChange,
  onClickSave,
  onClickCancel
}) => {
  return <div className="FormContact">
    <label>
      Name:
      <input 
        type="text"
        name="name"
        value={contact.name}
        onChange={onChange}
      />
    </label>
    <label>
      Phone:
      <input 
        type="number"
        name="phone"
        value={contact.phone}
        onChange={onChange}
      />
    </label>
    <label>
      Email:
      <input 
        type="email"
        name="email"
        value={contact.email}
        onChange={onChange}
      />
    </label>
    <label>
      Photo:
      <input 
        type="text"
        name="photo"
        value={contact.photo}
        onChange={onChange}
      />
    </label>
    <p className="Photo">
      <span>Photo preview: </span>
      <div className="Photo_block">
        {contact.photo !== "" 
          ? <img src={contact.photo} alt=""/> 
          : <></>
        }
      </div>
    </p>
    <div>
      <button className="Save" onClick={onClickSave}>
        Save
      </button>
      <button className="Cancel" onClick={onClickCancel}>
        Back to contacts
      </button>
    </div>
    
  </div>
};

export default FormContact;
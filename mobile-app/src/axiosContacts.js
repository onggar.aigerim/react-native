import axios from "axios";

const axiosContacts = axios.create({
  baseURL: "https://js-aigerim-onggar-default-rtdb.firebaseio.com/"
});

export default axiosContacts;
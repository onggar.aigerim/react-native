import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import FormContact from "../../components/FormContact/FormContact";
import Loader from "../../components/UI/Loader/Loader";
import { addContact } from "../../store/actions";

const AddNewContact = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.loading);

  const [contact, setContact] = useState({
    name: "",
    phone: "",
    email: "",
    photo: ""
  });

  const onChangeHandler = (e) => {
    const { name, value } = e.target;
    setContact(prevState => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const cancelHandler = () => {
    navigate("/");
  };

  const addNewContact = () => {
    if (contact.name !== "" && contact.phone !== "" && contact.email !== "" && contact.photo !== "") {
      dispatch(addContact(contact, navigate));
    };
  };

  return <div>
    <Loader loading={loading}/>
    <FormContact 
      contact={contact}
      onChange={onChangeHandler}
      onClickSave={addNewContact}
      onClickCancel={cancelHandler}
      photo={contact.photo}
    />
  </div> 
};

export default AddNewContact;
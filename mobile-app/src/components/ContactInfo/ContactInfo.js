import { Image, Pressable, StyleSheet, Text, View } from "react-native";

const ContactInfo = ({contact, onPress}) => {
  return <View style={styles.container}>
    <View style={styles.modalView}>
      <Text style={styles.title}>Contact Info</Text>
      <Text style={styles.contactName}>{contact.name}</Text>
      <Image style={styles.image} source={{uri: contact.photo}}></Image>
      <Text style={styles.infoText}>&#9742; {contact.phone}</Text>
      <Text style={styles.infoText}>&#128231; {contact.email}</Text>
      <Pressable
        style={[styles.button, styles.buttonClose]}
        onPress={onPress}
      >
        <Text style={styles.textStyle}>Cancel</Text>
      </Pressable>
    </View>
  </View>
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  modalView: {
    backgroundColor: "white",
    height: "100%",
    width: "100%",
    paddingHorizontal: 20
  },
  button: {
    borderRadius: 20,
    padding: 15,
    elevation: 2,
    backgroundColor: "#000",
    width: "100%",
    position: "absolute",
    bottom: 10,
    right: 20,
    left: 20
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 20,
  },
  title: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 30
  },
  contactName: {
    fontSize: 50,
    marginVertical: 10
  },
  infoText: {
    fontSize: 25,
    marginVertical: 10
  },
  image: {
    width: 150, 
    height: 150, 
    marginRight: 10
  },
});

export default ContactInfo;
